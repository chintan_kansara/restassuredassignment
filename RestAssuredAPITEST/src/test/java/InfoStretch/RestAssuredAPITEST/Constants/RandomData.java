package InfoStretch.RestAssuredAPITEST.Constants;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomData {

	public static String randomEmail = RandomStringUtils.randomAlphabetic(5)+"@example.com" ;
	public static String randomPassword = RandomStringUtils.randomAlphabetic(10);
	public static String randomFirstName= RandomStringUtils.randomAlphabetic(6);
	public static String randomLastName= RandomStringUtils.randomAlphabetic(6);
	public static String randomDetails= RandomStringUtils.randomAlphabetic(8);
	public static String randomName = RandomStringUtils.randomAlphabetic(6);
		
}
