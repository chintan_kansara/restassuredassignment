package InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomSystemRoles {

    public CustomSystemRoles(Data data) {
		super();
		this.data = data;
	}

	@SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
