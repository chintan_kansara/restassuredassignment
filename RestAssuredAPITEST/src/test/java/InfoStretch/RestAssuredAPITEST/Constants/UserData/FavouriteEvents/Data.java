
package InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    public Data(String type, Relationships relationships) {
		this.type = type;
		this.relationships = relationships;
	}

	@SerializedName("type")
    @Expose
    private String type;
    @SerializedName("relationships")
    @Expose
    private Relationships relationships;

    public String getType() {
        return type;
    }

	public void setType(String type) {
        this.type = type;
    }

    public Relationships getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }

}
