package InfoStretch.RestAssuredAPITEST.Constants.UserData.Users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data_User {

	public Data_User() {}
	
	public Data_User(Attributes attributes, String type) {
		this.attributes = attributes;
		this.type = type;
	}
	
	@SerializedName("attributes")
	@Expose
	private Attributes attributes;
	@SerializedName("type")
	@Expose
	private String type;

	public Attributes getAttributes() {
		return attributes;
	}

	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
