package InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {
	
    public Event(Data_ data) {
		this.data = data;
	}

	@SerializedName("data")
    @Expose
    private Data_ data;

    public Data_ getData() {
        return data;
    }

    public void setData(Data_ data) {
        this.data = data;
    }

}
