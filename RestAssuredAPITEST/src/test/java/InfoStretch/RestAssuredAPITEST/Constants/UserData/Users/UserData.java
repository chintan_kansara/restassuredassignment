package InfoStretch.RestAssuredAPITEST.Constants.UserData.Users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

	public UserData(Data_User data) {
		
		this.data = data;
	}

	@SerializedName("data")
	@Expose
	private Data_User data;

	public Data_User getData() {
		return data;
	}

	public void setData(Data_User data) {
		this.data = data;
	}

}
