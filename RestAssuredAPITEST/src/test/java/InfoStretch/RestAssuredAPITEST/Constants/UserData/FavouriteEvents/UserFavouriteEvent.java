package InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserFavouriteEvent {

    public UserFavouriteEvent(Data data) {
		this.data = data;
	}

	@SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
