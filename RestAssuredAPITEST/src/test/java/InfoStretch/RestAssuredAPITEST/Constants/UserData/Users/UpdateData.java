package InfoStretch.RestAssuredAPITEST.Constants.UserData.Users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateData {

	public UpdateData(UpdateAttributes attributes, String type, String id) {
		this.attributes = attributes;
		this.type = type;
		this.id = id;
	}
	
	@SerializedName("attributes")
	@Expose
	private UpdateAttributes attributes;
	@SerializedName("type")
	@Expose
	private String type;

	@SerializedName("id")
	@Expose
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UpdateAttributes getAttributes() {
		return attributes;
	}

	public void setAttributes(UpdateAttributes attributes) {
		this.attributes = attributes;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
