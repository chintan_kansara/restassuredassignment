
package InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

	 public Data(Attributes attributes, String type,String id) {
			super();
			this.attributes = attributes;
			this.type = type;
			this.id = id;
		}
	
    public Data(Attributes attributes, String type) {
		super();
		this.attributes = attributes;
		this.type = type;
	}

	@SerializedName("attributes")
    @Expose
    private Attributes attributes;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
