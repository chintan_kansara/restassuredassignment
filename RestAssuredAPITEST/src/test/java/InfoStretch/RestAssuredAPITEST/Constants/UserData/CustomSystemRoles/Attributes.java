package InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {

    public Attributes(String name) {
		super();
		this.name = name;
	}

	@SerializedName("name")
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
