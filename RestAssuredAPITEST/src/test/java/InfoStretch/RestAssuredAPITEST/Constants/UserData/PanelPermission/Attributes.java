package InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Attributes {

    public Attributes(Boolean canAccess, String panelName) {
		super();
		this.canAccess = canAccess;
		this.panelName = panelName;
	}

    @JsonProperty("can-access")
    private Boolean canAccess;
    @JsonProperty("panel-name")
    private String panelName;

    public Boolean getCanAccess() {
        return canAccess;
    }

    public void setCanAccess(Boolean canAccess) {
        this.canAccess = canAccess;
    }

    public String getPanelName() {
        return panelName;
    }

    public void setPanelName(String panelName) {
        this.panelName = panelName;
    }

}
