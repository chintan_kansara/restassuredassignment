package InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Relationships {

    public Relationships(Event event) {
		this.event = event;
	}

	@SerializedName("event")
    @Expose
    private Event event;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

}
