package InfoStretch.RestAssuredAPITEST.Constants.UserData.Users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateAttributes {

	public UpdateAttributes(String email,String firstName) {
		this.email = email;
		this.firstName = firstName;
	}
	
	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("first-name")
	@Expose
	private String firstName;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}	
	
}
