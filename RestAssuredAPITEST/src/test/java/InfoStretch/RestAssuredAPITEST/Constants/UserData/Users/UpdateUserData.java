package InfoStretch.RestAssuredAPITEST.Constants.UserData.Users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUserData {

public UpdateUserData(UpdateData data) {
		
		this.data = data;
	}

	@SerializedName("data")
	@Expose
	private UpdateData data;

	public UpdateData getData() {
		return data;
	}

	public void setData(UpdateData data) {
		this.data = data;
	}

	
}
