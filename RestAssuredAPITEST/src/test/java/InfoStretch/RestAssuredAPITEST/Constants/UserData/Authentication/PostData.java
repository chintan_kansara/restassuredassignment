package InfoStretch.RestAssuredAPITEST.Constants.UserData.Authentication;

public class PostData {

	private String email;
	private String password;
	
	public PostData(String email,String password) {
		this.email = email;
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}

}
