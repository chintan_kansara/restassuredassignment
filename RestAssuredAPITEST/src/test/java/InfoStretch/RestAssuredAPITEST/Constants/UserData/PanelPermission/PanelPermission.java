
package InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PanelPermission {

    public PanelPermission(Data data) {
		super();
		this.data = data;
	}

	@SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
