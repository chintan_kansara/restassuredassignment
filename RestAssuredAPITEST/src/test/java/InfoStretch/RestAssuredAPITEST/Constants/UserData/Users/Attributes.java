package InfoStretch.RestAssuredAPITEST.Constants.UserData.Users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {
	
	public Attributes(String email, String password, String avatarUrl, String firstName, String lastName,
			String details, String contact, String facebookUrl, String twitterUrl, String instagramUrl,
			String googlePlusUrl, String originalImageUrl) {

		this.email = email;
		this.password = password;
		this.avatarUrl = avatarUrl;
		this.firstName = firstName;
		this.lastName = lastName;
		this.details = details;
		this.contact = contact;
		this.facebookUrl = facebookUrl;
		this.twitterUrl = twitterUrl;
		this.instagramUrl = instagramUrl;
		this.googlePlusUrl = googlePlusUrl;
		this.originalImageUrl = originalImageUrl;
	}

	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("password")
	@Expose
	private String password;
	@SerializedName("avatar_url")
	@Expose
	private String avatarUrl;
	@SerializedName("first-name")
	@Expose
	private String firstName;
	@SerializedName("last-name")
	@Expose
	private String lastName;
	@SerializedName("details")
	@Expose
	private String details;
	@SerializedName("contact")
	@Expose
	private String contact;
	@SerializedName("facebook-url")
	@Expose
	private String facebookUrl;
	@SerializedName("twitter-url")
	@Expose
	private String twitterUrl;
	@SerializedName("instagram-url")
	@Expose
	private String instagramUrl;
	@SerializedName("google-plus-url")
	@Expose
	private String googlePlusUrl;
	@SerializedName("original-image-url")
	@Expose
	private String originalImageUrl;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getTwitterUrl() {
		return twitterUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public String getInstagramUrl() {
		return instagramUrl;
	}

	public void setInstagramUrl(String instagramUrl) {
		this.instagramUrl = instagramUrl;
	}

	public String getGooglePlusUrl() {
		return googlePlusUrl;
	}

	public void setGooglePlusUrl(String googlePlusUrl) {
		this.googlePlusUrl = googlePlusUrl;
	}

	public String getOriginalImageUrl() {
		return originalImageUrl;
	}

	public void setOriginalImageUrl(String originalImageUrl) {
		this.originalImageUrl = originalImageUrl;
	}

}
