package InfoStretch.RestAssuredAPITEST.Constants;

public class UriData {
	
	public static final String baseUri = "http://qe.events.infostretch.com/api/v1";
	public static final String authLogin = "/auth/login";
	public static final String freshLogin = "/auth/fresh-login";
	public static final String users = "/users";
	public static final String importJob = "/import-jobs";
	public static final String userFavouriteEvents = "/user-favourite-events";
	public static final String panelPermissions = "/panel-permissions";
	public static final String customSystemRoles = "/custom-system-roles";
	
}

