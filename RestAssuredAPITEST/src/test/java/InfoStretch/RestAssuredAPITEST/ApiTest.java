package InfoStretch.RestAssuredAPITEST;

import static InfoStretch.RestAssuredAPITEST.Constants.UriData.baseUri;
import static InfoStretch.RestAssuredAPITEST.Constants.UriData.customSystemRoles;
import static InfoStretch.RestAssuredAPITEST.Constants.UriData.importJob;
import static InfoStretch.RestAssuredAPITEST.Constants.UriData.panelPermissions;
import static InfoStretch.RestAssuredAPITEST.Constants.UriData.userFavouriteEvents;
import static InfoStretch.RestAssuredAPITEST.Constants.UriData.users;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import InfoStretch.RestAssuredAPITEST.Constants.UserData.Authentication.PostData;
import InfoStretch.RestAssuredAPITEST.Constants.RandomData;
import InfoStretch.RestAssuredAPITEST.Constants.UriData;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.Users.Attributes;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents.Data;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents.Data_;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.Users.Data_User;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents.Event;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents.Relationships;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.Users.UpdateAttributes;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.Users.UpdateData;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.Users.UpdateUserData;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.Users.UserData;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.FavouriteEvents.UserFavouriteEvent;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.CustomSystemRoles;
import InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.PanelPermission;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.response.Response;

public class ApiTest {

	private String token;
	private String id;
	private String eventId;
	private String panelPermissionsid;
	private String customSystemRolesid;

	@BeforeTest
	public void setBaseURI() {
		RestAssured.baseURI = baseUri;
	}

	@Test(priority=1 ,description = "Post Request - get token from response")
	public void Test1() {
		PostData data = new PostData("admin@mailinator.com", "admin123#");
		Response res = (Response) given().contentType("application/json").body(data).when().post(UriData.authLogin)
				.then().assertThat().statusCode(200).log().body().extract().response();
		token = res.path("access_token").toString();

	}

	@Test(priority=2,description = "Post Request - for fresh login")
	public void Test2() {
		PostData data = new PostData("admin@mailinator.com", "admin123#");
		given().contentType("application/json").header("Authorization", "JWT " + token).body(data).when()
				.post(UriData.freshLogin).then().statusCode(200).log().body().extract().response();
	}

	@Test(priority=3,description = "Get Request - Get a list of Users")
	public void Test3() {
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).queryParam("sort", "email")
				.when().get(users).then().statusCode(200).log().body().extract().response();
	}

	@Test(priority=4,description = "Post Request - Create a new user using an email, password and an optional name")
	public void Test4() {
		Attributes userdata = new Attributes(RandomData.randomEmail, RandomData.randomPassword,
				"http://example.com/example.png", RandomData.randomFirstName, RandomData.randomLastName,
				RandomData.randomDetails, "example972", "http://facebook.com/facebook", "http://twitter.com/twitter",
				"http://instagram.com/instagram", "http://plus.google.com/plus.google",
				"https://cdn.pixabay.com/photo/2013/11/23/16/25/birds-216412_1280.jpg");
		Data_User data1 = new Data_User(userdata, "user");
		UserData data = new UserData(data1);
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response response = given().contentType("application/vnd.api+json").queryParam("sort", "email").body(data).log()
				.all().when().post(users).then().statusCode(201).log().body().extract().response();
		id = response.path("data.id").toString();
	}

	@Test(priority=5,description = "Get Request - Get a single user")
	public void Test5() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when()
				.get(users + "/" + Integer.parseInt(id)).then().statusCode(200).log().body().extract().response();
	}

	@Test(priority=6,description = "Patch Request - Update a single user by setting the email, email and/or name")
	public void Test6() {
		UpdateAttributes userdata = new UpdateAttributes(RandomData.randomEmail, RandomData.randomFirstName);
		String updatedEmail = userdata.getEmail();
		UpdateData data1 = new UpdateData(userdata, "user", id);
		UpdateUserData data = new UpdateUserData(data1);
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().contentType("application/vnd.api+json").header("Authorization", "JWT " + token).log().all().body(data)
				.when().patch(users + "/" + Integer.parseInt(id)).then().statusCode(200).log().body().extract()
				.response();
		assertEquals(res.path("data.attributes.email"),updatedEmail);
	}

	@Test(priority=7,description = "Delete Request - Delete a single user")
	public void Test7() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when()
				.delete(users + "/" + Integer.parseInt(id)).then().statusCode(200).log().body().extract().response();
		assertEquals(res.path("meta.message"), "Object successfully deleted");
	}

	@Test(priority=8,description = "Get Request - Get a list of all import jobs")
	public void Test8() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when().get(importJob).then()
				.statusCode(200).log().body().extract().response();
	}

	@Test(priority=9,description = "Get Request - Favourite Events Collection")
	public void Test9() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().header("Authorization", "JWT " + token).when().get(userFavouriteEvents).then().statusCode(200).log()
				.body().extract().response();
	}

	@Test(priority=10,description = "Post Request - Favourite Events Collection")
	public void Test10() {
		Data_ data_ = new Data_("55", "event");
		eventId = data_.getId();
		Event event = new Event(data_);
		Relationships rs = new Relationships(event);
		Data data = new Data("user-favourite-event", rs);
		UserFavouriteEvent favouriteEvent = new UserFavouriteEvent(data);
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().header("Authorization", "JWT " + token).contentType("application/vnd.api+json").body(favouriteEvent)
				.log().all().when().post(userFavouriteEvents).then().statusCode(201).log().body().extract().response();
	}

	@Test(priority=11,description = "Get Request - Favourite Events Detail")
	public void Test11() {
		given().header("Authorization", "JWT " + token).log().all().when().get(userFavouriteEvents + "/" + eventId)
				.then().statusCode(200).log().body().extract().response();
	}

	@Test(priority=12,description = "Delete Request - Delete Event")
	public void Test12() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().header("Authorization", "JWT " + token).log().all().when().delete(userFavouriteEvents + "/" + eventId)
				.then().statusCode(200).log().body().extract().response();
		assertEquals(res.path("meta.message"), "Object successfully deleted");
	}

	@Test(priority=13,description = "Get Request - Get a list of all Panel Permissions")
	public void Test13() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when().get(panelPermissions)
				.then().statusCode(200).log().body().extract().response();
	}

	@Test(priority=14,description = "Post Request - Create a new panel permission using a name")
	public void Test14() {
		InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Attributes attributes = new InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Attributes(
				true, RandomData.randomName);
		InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Data data = new InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Data(
				attributes, "panel-permission");
		PanelPermission pp = new PanelPermission(data);
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().contentType("application/vnd.api+json").header("Authorization", "JWT " + token).body(pp)
				.log().all().when().post(panelPermissions).then().statusCode(201).log().body().extract().response();
		panelPermissionsid = res.path("data.id").toString();
	}

	@Test(priority=15,description = "Get Request - Get a single panel permission")
	public void Test15() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when()
				.get(panelPermissions + "/" + Integer.parseInt(panelPermissionsid)).then().statusCode(200).log().body()
				.extract().response();
	}

	@Test(priority=16,description = "Patch Request - Update a single panel permission by setting the name and/or access type")
	public void Test16() {
		InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Attributes attributes = new InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Attributes(
				true, RandomData.randomName);
		String updatedPanelName = attributes.getPanelName();
		InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Data data = new InfoStretch.RestAssuredAPITEST.Constants.UserData.PanelPermission.Data(
				attributes, "panel-permission",panelPermissionsid);
		PanelPermission pp = new PanelPermission(data);
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().contentType("application/vnd.api+json").header("Authorization", "JWT " + token).body(pp).log().all()
				.when().patch(panelPermissions + "/"+panelPermissionsid).then().statusCode(200)
				.log().body().extract().response();
		assertEquals(res.path("data.id"),panelPermissionsid);
		assertEquals(res.path("data.attributes.panel-name"),updatedPanelName);
		
	}

	@Test(priority=17,description = "Delete Request - Delete a single panel permission")
	public void Test17() {

		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).log().all().when()
				.delete(panelPermissions + "/" + Integer.parseInt(panelPermissionsid)).then().statusCode(200).log()
				.body().extract().response();
		assertEquals(res.path("meta.message"), "Object successfully deleted");
	}

	@Test(priority=18,description = "Get Request - Get the details of the panel permission")
	public void Test18() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when()
				.get(customSystemRoles + "/" +3+ panelPermissions).then()
				.statusCode(200).log().body().extract().response();
	}

	@Test(priority=19,description = "Get Request - Get a list of all Custom System Roles")
	public void Test19() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when().get(customSystemRoles)
				.then().statusCode(200).log().body().extract().response();
	}

	@Test(priority=20,description = "Post Request - Create a new custom system role using a name")
	public void Test20() {
		InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Attributes attributes = new InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Attributes(
				RandomData.randomName);
		InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Data data = new InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Data(
				attributes, "custom-system-role");
		CustomSystemRoles pp = new CustomSystemRoles(data);
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().contentType("application/vnd.api+json").header("Authorization", "JWT " + token).body(pp)
				.log().all().when().post(customSystemRoles).then().statusCode(201).log().body().extract().response();
		customSystemRolesid = res.path("data.id").toString();
	}

	@Test(priority=21,description = "Get Request - Get a single custom system role")
	public void Test21() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when()
				.get(customSystemRoles + "/" + Integer.parseInt(customSystemRolesid)).then().statusCode(200).log()
				.body().extract().response();
	}

	@Test(priority=22,description = "Patch Request - Update a single custom system role")
	public void Test22() {
		InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Attributes attributes = new InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Attributes(
				RandomData.randomName);
		String updatedName = attributes.getName();
		InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Data data = new InfoStretch.RestAssuredAPITEST.Constants.UserData.CustomSystemRoles.Data(
				attributes, "custom-system-role", customSystemRolesid);
		CustomSystemRoles pp = new CustomSystemRoles(data);
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().contentType("application/vnd.api+json").header("Authorization", "JWT " + token).body(pp).log().all()
				.when().patch(customSystemRoles + "/" + Integer.parseInt(customSystemRolesid)).then().statusCode(200)
				.log().body().extract().response();
		assertEquals(res.path("data.id"),customSystemRolesid );
		assertEquals(res.path("data.attributes.name"),updatedName);
	}

	@Test(priority=23,description = "Delete Request - Delete a single custom system")
	public void Test23() {

		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		Response res = given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).log().all().when()
				.delete(customSystemRoles + "/" + Integer.parseInt(customSystemRolesid)).then().statusCode(200).log()
				.body().extract().response();
		assertEquals(res.path("meta.message"),"Object successfully deleted");
	}

	@Test(priority=24,description = "Get Request - Get the details of the custom system roles for a panel permission")
	public void Test24() {
		RestAssured.config = RestAssured.config().encoderConfig(
				EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
		given().accept("application/vnd.api+json").header("Authorization", "JWT " + token).when()
				.get(panelPermissions + "/" + Integer.parseInt(customSystemRolesid) + customSystemRoles).then()
				.statusCode(200).log().body().extract().response();
	}

}
